# Soal-Shift-Sisop-Modul-2-C04-2022 

### Anggota Kelompok
* Meisya Salsabila Indrijo Putri  5025201114
* Muhammad Haikal Aria Sakti 05111940000088
* Marcelino Salim 5025201026

### Kendala Keseluruhan:
Saya mengerjakan sendiri mulai dari awal pratikum -> mengerjakan soal, membuat readme, commit, demo, dan revisi. (Meisya Salsabila Indrijo Putri / 5025201114)

# Soal 2
Membuat path agar memudahkan saat pemanggilan.

	char path[50];
	char zipfile[50];
	strcpy(path, "/home");
	strcat(path, "/meisyalsabila/shift2/drakor/");
	strcpy(zipfile, "/home");
	strcat(zipfile, "/meisyalsabila/shift2/drakor.zip");

### 2A
Pertama kita diminta untuk membuat folder `/home/[user]/shift2/drakor`.

    char *argv[] = {"mkdir", "-p", path, NULL};
    execv("/bin/mkdir", argv);

Lalu mengextract file `drakor.zip` dan menghapus folder-folder yang tidak dibutuhkan.

    char *argv[] = { "unzip", "-o", zipfile, "-d", path, "*.png", NULL };
    execv ("/usr/bin/unzip", argv);

Disini saya menggunakan `*.png` agar nantinya file yang terextract hanya file yang berjenis png. Sehingga folder-folder yang tidak dibutuhkan akan terhapus secara langsung.

### 2B
Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang sudah terextract. Karena tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan genre pada poster drama korea.

	struct drakor {
		char name_file[100];
		char title[100];
		char genre[100];
		int year;
	};

`struct drakor` untuk menyimpan setiap data dari poster drakor yang tersedia.

	struct drakor data[100];
	struct dirent *dp;
	DIR *dir = opendir(path);

	while ((dp = readdir(dir)) != NULL){
		if(strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") != 0){
			int flag1 = 0;
			strcpy(data[flag].name_file, dp->d_name);
			
			if(strchr(data[flag].name_file, '_')!= 0) flag1++;
				
			char *tok = strtok(dp->d_name, "_;.");	
			for(int i=0; tok!=NULL; i++){
				if(i==0) strcpy(data[flag].title, tok);
				else if (i==1) data[flag].year=atoi(tok);
				else if (i==2) strcpy(data[flag].genre, tok);
				else if (i==3) {
					flag++;
						
					if(flag1!=0){
						strcpy(data[flag].name_file, data[flag-1].name_file);
						strcpy(data[flag].title, tok);
						flag1=0;
					}
					i=0;
				}
				tok = strtok (NULL, "_;.");
			}	
		}
	}

Membaca semua file path dan mengambil setiap data dari name file. Lalu, memasukkan setiap data kedalam `struct drakor` yang telah diinialisasi sebelumnya, gunakan juga `strtok` untuk mengambil data title, year, dan genre dari setiap file yang tersedia. Setelah didapat semua data pada `path`, kirim data dan looping sebanyak jumlah flag untuk membuat folder dengan melengkapi path sesuai genre apa saja yang tersedia dalam setiap nama file di `path`.

    void folder_genre(struct drakor *ptr, int index, char path[]){
		char tmp[100];
		strcpy(tmp, path);
		strcat(tmp, "/");
		strcat(tmp, (ptr+index)->genre);
		...
		char *argv[] = {"mkdir", "-p", tmp, NULL};
		execv("/bin/mkdir", argv);
		...
    }

Membuat fungsi `folder_genre` untuk buat folder sesuai dengan genrenya menggunakan `mkdir -p` dan dipanggil pada int main.

	for(int i=0; i<flag; i++) folder_genre(data, i, path);


### 2C
Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama `/drakor/romance/start-up.png`. Disini saya membuat fungsi `move_genre` agar memudahkan pada saat pemanggilan.

    void move_genre(struct drakor *ptr, int index, char path[]){
		char from[100];
		strcpy(from, path);
		strcat(from, "/");
		strcat(from, (ptr+index)->name_file);
	
		char destination[100];
		strcpy(destination, path);
		strcat(destination, (ptr+index)->genre);
		strcat(destination, "/");
		strcat(destination, (ptr+index)->title);
		strcat(destination, ".png");
		...
		if(child2_id == 0){
		    char *argv[] = {"cp", from, destination, NULL};
		    execv("/usr/bin/cp", argv);
		} else {
		    while ((wait(&status)) > 0);
		    return;
		}
    }

Membuat fungsi `move_genre` untuk memindahkan folder png sesuai dengan genre pada title poster dengan membuat path terlebih dahulu untuk mencatat asal file dengan memasukkan nama filenya dan memasukkan genre file sebagai foldernya dan ubah nama file dengan data title dari masing-masing file tersebut. Tetapi disini, saya men-copy dahulu poster dengan command `cp` dari path `from` dan di paste ke dalam `destination`.

### 2D
Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus dipindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama `“start-up;2020;romance_the-k2;2016;action.png”` dipindah ke folder `“/drakor/romance/start-up.png”` dan `“/drakor/action/the-k2.png”`.

	int flag1 = 0;
		...
		if(strchr(data[flag].name_file, '_')!= 0) flag1++;
		...
			if(flag1!=0){
				strcpy(data[flag].name_file, data[flag-1].name_file);
				strcpy(data[flag].title, tok);
				flag1=0;
			}
	...

Seperti yang sudah dijelaskan pada poin B, untuk case ini memakai `strchr` untuk mencari nama file yang menunjukkan dua poster sekaligus, tandai file tersebut dan catat data tersebut dengan nama file yang sama pada array of struct setelahnya. Jadi, setelah didapatkan dua nama file yang sama pada beberapa iterasi yang berurutan, data nama poster tersebut digunakan untuk mencatat path `from` pada poin C sebelumnya dan didapatkan dua file dengan folder genre di masing-masing poster pada file tersebut. Setelah itu, fungsi `move_genre` dipanggil pada int main.

	for(int i=0; i<flag; i++) move_genre(data, i, path);

### 2E
Membuat file `data.txt` di setiap folder genre yang berisikan nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending).

	void list_genre(struct drakor *ptr, int flag){
		struct drakor list[100];
		int index_list = flag;
		
		for(int i=0; i<flag; i++){
			strcpy(list[i].genre, (ptr+i)->genre);
		}
		
		for(int i=0; i<index_list; i++){
			for(int j=i+1; j<index_list; j++){
				if(strcmp(list[i].genre, list[j].genre)==0){
					for(int k=j; k<index_list; k++){
						strcpy(list[k].genre, list[k+1].genre);
					}
					j--;
					index_list--;
				}	
			}
		}	
	
		printf("Ada %d genre, yaitu : ", index_list);
		for(int i=0; i<index_list; i++){
			printf("%s\n", list[i].genre);
		}

Membuat fungsi `list_genre` yang digunakan untuk menghapus duplicate genre, mengelist dan mengecek genre, setelah diiterasi cek dan di-list maka selanjutnya akan di tuliskan pada file txt yang nantinya akan berada disetiap folder genre.

	for(int i=0; i<index_list; i++){
		struct drakor tmp[100];
		int index_tmp=0;
		
		for(int j=0; j<flag; j++){
			if(strcmp(list[i].genre, (ptr+j)->genre)==0){
				strcpy(tmp[index_tmp].title, (ptr+j)->title);
				tmp[index_tmp].year=(ptr+j)->year;
				index_tmp++;
			}
		}
		
		for(int j=0; j<index_tmp-1; j++){
			int min_id=j;
			for(int k=j+1; k<index_tmp; k++){
				if(tmp[k].year<tmp[min_id].year){
				min_id=k;
				struct drakor tmp2;
				strcpy(tmp2.title,tmp[min_id].title);
				tmp2.year=tmp[min_id].year;
				strcpy(tmp[min_id].title, tmp[j].title);
				tmp[min_id].year=tmp[j].year;
				strcpy(tmp[j].title, tmp2.title);
				tmp[j].year=tmp2.year;
				}		
			}
		}
		
		char text_path[100];
		strcpy(text_path, "/home/meisyalsabila/shift2/drakor/");
		strcat(text_path, list[i].genre);
		strcat(text_path, "/data.txt");
		...
			char *argv[] = {"touch", text_path, NULL};
			execv("/usr/bin/touch", argv);
    	...
		char isi_text[1000];
		strcpy(isi_text, "kategori : ");
		strcat(isi_text, list[i].genre);
		
		for(int i=0; i<index_tmp; i++){
			strcat(isi_text, "\n\n");
			strcat(isi_text, "nama : ");
			strcat(isi_text, tmp[i].title);
			strcat(isi_text, "\n");
			strcat(isi_text, "rilis : ");
			char movie_year[10];
			sprintf(movie_year, "%d", (tmp[i].year));
			strcat(isi_text, movie_year);
		}
		FILE *fptr=fopen(text_path, "a");
		fputs(isi_text, fptr);
		fclose(fptr);

Menentukan destination path file txt `text_path` dimana sesuai dengan genre masing-masing drama korea dan menuliskan text dengan format yang sudah ditentukan `isi_text`, yakni kategori, nama, lalu tahun rilis yang mana nantinya akan diurutkan secara ascending berdasarkan tahun rilisnya. Setelah itu, file txt akan diletakkan ke `text_path` menggunakan `fputs`.

### Kendala dan Dokumentasi:
* Sempat bingung ketika mau memisahkan dua genre film yang berada dalam 1 poster.
* Kesulitan pada saat install gcc

![Screenshot_from_2022-03-26_16-21-38](/uploads/1ba9489e1124767215589c283ce7818c/Screenshot_from_2022-03-26_16-21-38.png)
![Screenshot_from_2022-03-26_16-22-10](/uploads/77ecff1e85f72b45f3e36ef433048ad5/Screenshot_from_2022-03-26_16-22-10.png)
![Screenshot_from_2022-03-27_06-36-39](/uploads/d8df7a81d425375d163f3b1aa0704824/Screenshot_from_2022-03-27_06-36-39.png)


# Soal 3
Membuat path terlebih dahulu untuk memudahkan saat pemanggilan.

	char path[50];
		strcpy(path, "/home");
		strcat(path, "/meisyalsabila/modul2");
	char air[50]; 
    	strcpy(air, path); 
    	strcat(air, "/air"); 
	char darat[50]; 
    	strcpy(darat, path); 
    	strcat(darat, "/darat");
	char zipfile[50]; 
    	strcpy(zipfile, path); 
    	strcat(zipfile, "/animal.zip");
	char animal[50]; 
    	strcpy(animal,path);
    	strcat(animal,"/animal");
	char list[50]; 
    	strcpy(list,air); 
    	strcat(list,"/list.txt");

### 3A
Pertama diminta untuk membuat 2 directory di `/home/[USER]/modul2/` dengan nama `darat` lalu 3 detik kemudian membuat directory ke 2 dengan nama `air`.

	if (child == 0){
    	char *argv[] = {"mkdir", "-p", darat, NULL};
    	execv("/bin/mkdir", argv);
    	sleep(3);
	} 
	...
	if(child2 == 0){
    	char *argv[] = {"mkdir", "-p", air, NULL};
    	execv("/bin/mkdir", argv);
    } 

Untuk memberikan jeda 3 detik di antara pembuatan 2 directory, disini saya menggunakan `sleep(3);`.

### 3B
Kemudian program diminta dapat melakukan extract `animal.zip` di `/home/[USER]/modul2/`.

	if(child3 == 0){
    	char *argv[] = {"unzip", zipfile, "-d", path, NULL};
    	execv("/bin/unzip", argv);
    } 	

### 3C
Memisahkan hasil extract sesuai kategori menjadi hewan darat dan hewan air sesuai dengan nama filenya. Dimasukkan ke folder `/home/[USER]/modul2/darat` dan `/home/[USER]/modul2/air`. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

	if(child4 == 0){
    	char *argv[] = {"find", animal, "-name", "*darat*", "-exec", "mv", "-t", darat, "{}", "+", NULL};
    	execv("/bin/find", argv);
    	sleep(3);
    } 
    ...
	if(child5 == 0){
    	char *argv[] = {"find", animal, "-name", "*air*", "-exec", "mv", "-t", air, "{}", "+", NULL};
    	execv("/bin/find", argv);
    } 

Dalam memindahkan file menggunakan kombinasi command `find` dan `mv`, serta untuk memberi jeda dapat menggunakan `sleep()`. Lalu dikarenakan hewan air dan darat dalam folder animal telah dipindah, maka hanya tersisa hewan yang tanpa keterangan air / darat. Sehingga, penghapusan dapat diartikan sebagai menghapus file yang tersisa di dalam folder animal. Kita dapat menggunakan command `find` dan `rm`.

	if(child6 == 0){    
    	char *argv[] = {"find", animal, "-type", "f", "-name", "*.jpg", "-exec", "rm", "-f", "{}", "+", NULL};
    	execv("/bin/find", argv);             
    } 

### 3D
Menghapus semua burung yang ada di directory `/home/[USER]/modul2/darat`, hewan burung ditandai dengan adanya `bird` pada nama file. Kita dapat menggunakan command `find` dan `rm`.

	if(child7 == 0){
    	char *argv[] = {"find", darat, "-name", "*bird*", "-exec", "rm", "-rf", "-f", "{}", "+", NULL};
    	execv("/bin/find", argv);
    } 

### 3E
Membuat list nama semua hewan yang ada di directory `/home/[USER]/modul2/air` ke `list.txt` dengan format `UID_[UID file permission]_Nama File.[jpg/png]` dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

	if(child8 == 0){
    	char *argv[] = {"touch", list, NULL};
    	execv("/bin/touch", argv);
    } 
    ...
	if(child9 == 0){
		char *argv[] = {"find", air, "-name", "*.jpg", "-fprintf", list, "%u_%M_%f\n", NULL};
    	execv("/bin/find", argv);
    } 
    ...
	if(child10 == 0){
		char *argv[] = {"gawk", "-i", "inplace", "BEGIN{FS=\"-\"} {print $1\"-\"$2$(NF)}", list, NULL};
		execv("/bin/gawk", argv);
	} 
	else {
    	while((wait(&status)) > 0);
    	char *argv[] = {"gawk", "-i", "inplace", "BEGIN{FS=\"_\"} { print $1\"_\"$2\"_\"$3\".jpg\"}", list, NULL};
    	execv("/bin/gawk", argv);
    }

### Kendala dan Dokumentasi:
![Screenshot_from_2022-03-27_21-21-55](/uploads/e08c3e7555e1b33c4b496671e6c2d574/Screenshot_from_2022-03-27_21-21-55.png)
![Screenshot_from_2022-03-27_21-22-39](/uploads/0f2bb6e61afbc076bb43e80b24e27366/Screenshot_from_2022-03-27_21-22-39.png)
