#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <fcntl.h>

int main() { 
  char path[50];
	  strcpy(path, "/home");
	  strcat(path, "/meisyalsabila/modul2");
  char air[50]; 
    strcpy(air, path); 
    strcat(air, "/air");  
  char darat[50]; 
    strcpy(darat, path); 
    strcat(darat, "/darat");
  char zipfile[50]; 
    strcpy(zipfile, path); 
    strcat(zipfile, "/animal.zip");
  char animal[50]; 
    strcpy(animal,path);
    strcat(animal,"/animal");
  char list[50]; 
    strcpy(list,air); 
    strcat(list,"/list.txt");

  int status;
  pid_t child; 
  child = fork();

  if (child < 0){
    exit(EXIT_FAILURE);
  }

  if (child == 0){
    char *argv[] = {"mkdir", "-p", darat, NULL};
    execv("/bin/mkdir", argv);
    sleep(3);
  } 
  else {
    while((wait(&status)) > 0);
    pid_t child2; 
    child2 = fork();

      if(child2 == 0){
        char *argv[] = {"mkdir", "-p", air, NULL};
        execv("/bin/mkdir", argv);
      } 
      else {
        while((wait(&status)) > 0);
        pid_t child3; 
        child3 = fork();

        if(child3 == 0){
          char *argv[] = {"unzip", zipfile, "-d", path, NULL};
          execv("/bin/unzip", argv);
        } 
        else {
          while((wait(&status)) > 0);
          pid_t child4; 
          child4 = fork();

          if(child4 == 0){
            char *argv[] = {"find", animal, "-name", "*darat*", "-exec", "mv", "-t", darat, "{}", "+", NULL};
            execv("/bin/find", argv);
            sleep(3);
          } 
          else{
            while((wait(&status)) > 0);
            pid_t child5; 
            child5 = fork();

            if(child5 == 0){
              char *argv[] = {"find", animal, "-name", "*air*", "-exec", "mv", "-t", air, "{}", "+", NULL};
              execv("/bin/find", argv);
            } 
            else {
              while((wait(&status)) > 0);
              pid_t child6; 
              child6 = fork(); 

              if(child6 == 0){    
                char *argv[] = {"find", animal, "-type", "f", "-name", "*.jpg", "-exec", "rm", "-f", "{}", "+", NULL};
                execv("/bin/find", argv);             
              } 
              else {
                while((wait(&status)) > 0);
                pid_t child7; 
                child7 = fork();

              if(child7 == 0){
                char *argv[] = {"find", darat, "-name", "*bird*", "-exec", "rm", "-rf", "-f", "{}", "+", NULL};
                execv("/bin/find", argv);
              } 
              else {
                while((wait(&status)) > 0);
                pid_t child8; 
                child8 = fork();

                if(child8 == 0){
                  char *argv[] = {"touch", list, NULL};
                  execv("/bin/touch", argv);
                } 
                else {
                  while((wait(&status)) > 0);
                  pid_t child9; 
                  child9 = fork();
                  FILE *file_list = fopen(list, "r+");

                  if(child9 == 0){
                    char *argv[] = {"find", air, "-name", "*.jpg", "-fprintf", list, "%u_%M_%f\n", NULL};
                    execv("/bin/find", argv);
                  } 
                  else {
                    while((wait(&status)) > 0);
                    pid_t child10; 
                    child10 = fork();

                    if(child10 == 0){
                      char *argv[] = {"gawk", "-i", "inplace", "BEGIN{FS=\"-\"} {print $1\"-\"$2$(NF)}", list, NULL};
                      execv("/bin/gawk", argv);
                    } 
                    else {
                      while((wait(&status)) > 0);
                      char *argv[] = {"gawk", "-i", "inplace", "BEGIN{FS=\"_\"} { print $1\"_\"$2\"_\"$3\".jpg\"}", list, NULL};
                      execv("/bin/gawk", argv);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}